%global pname ydotool

Name:           %{pname}-systemd
Version:        0.1.0
Release:        1%{?dist}
Summary:        User service unit and required udev rules
BuildArch:      noarch

License:        GPLv3
URL:            https://gitlab.com/tofik-rpms/ydotool
Source0:        ydotool.service
Source1:        ydotool.rules

BuildRequires: systemd-rpm-macros
Requires:       %{pname} >= 1.0.0
%{?systemd_requires}

%description
Systemd user service file for ydotoold and required udev rules

%prep

%build


%install
install -D -m644 %{SOURCE0} %{buildroot}%{_userunitdir}/%{pname}.service
install -D -m644 %{SOURCE1} %{buildroot}%{_udevrulesdir}/60-%{pname}.rules

%check


%post
%systemd_user_post %{name}.service
%udev_rules_update

%preun
%systemd_user_preun %{name}.service

%postun
%systemd_user_postun_with_restart %{name}.service
%udev_rules_update


%files
%license
%doc
%{_userunitdir}/%{pname}.service
%{_udevrulesdir}/60-%{pname}.rules


%changelog
* Sat Jan 14 2023 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 0.1.0-1
- Intial build

